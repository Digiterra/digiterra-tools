#!/bin/bash

# Script for cloning remote database to local environment, using .env file for credentials
# Currently support only Mongodb database type without passwords

SCRIPT_DIR="$(dirname "$0")"
APP_DIR=$(cd $SCRIPT_DIR/..; pwd)

source "$APP_DIR/.env"
source "$APP_DIR/scripts/hosts.inc"

if [[ $APP_ENV == 'prod' ]]; then
  echo "Clone into production environment is disabled, exiting.
For unlock clone process change 'APP_ENV' variable in .env file."
  exit
fi

if [ -z $1 ]; then
  HOST='live'
else
  HOST=$1
fi

if [ -z ${HOSTS[$HOST]} ]; then
  echo "Error: host '$HOST' not found  in hosts.inc file."
  exit
fi

if [ -z ${DIRECTORIES[$HOST]} ]; then
  echo "Error: directory for  '$HOST' not set in hosts.inc file."
  exit
fi


DUMPFILE="/tmp/clone-remote_${HOST}_db-dump_`date -Iseconds`.archive"

DATABASE_NAME_REMOTE=`ssh ${HOSTS[$HOST]} "cd ${DIRECTORIES[$HOST]}; source ./.env; echo \\\$DATABASE_NAME"`

if [ -z $DATABASE_NAME_REMOTE ]; then
  echo "Error fetching remote database name from .env file at host ${HOSTS[$HOST]}/${DIRECTORIES[$HOST]}"
  exit
fi

echo "Importing remote db ${HOST}/$DATABASE_NAME_REMOTE to local/$DATABASE_NAME via $DUMPFILE file..."

ssh ${HOSTS[$HOST]} "cd ${DIRECTORIES[$HOST]}; source .env; mongodump -d $DATABASE_NAME_REMOTE --archive --gzip" > $DUMPFILE

mongorestore --archive=$DUMPFILE --gzip --drop --nsFrom "$DATABASE_NAME_REMOTE.*" --nsTo "$DATABASE_NAME.*"

echo "Importing remote db finished."

echo "RSyncing remote media to local..."

rsync -az --progress --delete ${HOSTS[$HOST]}:${DIRECTORIES[$HOST]}/public/uploads/ $APP_DIR/public/uploads/

echo "RSyncing remote media to local finished."

rm $DUMPFILE
