# Digiterra-tools

Script for cloning database and folders from remote host to local, using .env file

## Installing:

- Copy `scripts` folder to root of your project
- Copy `scripts/hosts.inc.example` to `scripts/hosts.inc` and fill right hosts and folders
- Configure access via SSH keys from current host to remote hosts


## Scripts:

### clone-remote.sh

Script for cloning database and folders from remote host to local, using .env file
